'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _server = require('./config/server');

var _server2 = _interopRequireDefault(_server);

var _staff = require('./app/routes/staff.route');

var _staff2 = _interopRequireDefault(_staff);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var path = '/api/v1/';

app.use(_bodyParser2.default.urlencoded({ extended: true }));
// app.use(morgan('dev'))
app.use(_bodyParser2.default.json());
app.use(path, _staff2.default);

app.get('/', function (req, res) {
    res.json('Hello Staff');
});

var port = process.env.PORT || 3001;
if (!module.parent) {
    app.listen(port, function () {
        console.log("Server is listening on port 3003");
    });
}
exports.default = app;