'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var information = new Schema({
    email: { type: String, required: false },
    internal_phone: { type: Number, required: false },
    personal_phone: { type: Number, required: false }
});

var Address = new Schema({
    address: { type: String, required: false },
    house_no: { type: String, required: false },
    soi: { type: String, required: false },
    road: { type: String, required: false },
    country: { type: String, required: false },
    province_id: { type: String, required: false },
    district: { type: String, required: false },
    sub_district_id: { type: String, required: false },
    post_code: { type: Number, required: false }
});

var staff = new Schema({
    staff_department: { type: String, required: true },
    staff_branch: { type: String, required: true },
    staff_title: { type: String, required: true },
    staff_fname: { type: String, required: false },
    staff_lname: { type: String, required: false },
    staff_gender: { type: String, required: false },
    staff_name: { type: String, required: false },
    address: Address,
    information: information,
    remark: { type: String, required: false }
}, {
    timestamps: true
});

var Staff = _mongoose2.default.model('Staff', staff);

exports.default = Staff;