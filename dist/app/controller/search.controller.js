"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _staff = require("../models/staff.model");

var _staff2 = _interopRequireDefault(_staff);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Searchstaff = function () {
    function Searchstaff() {
        _classCallCheck(this, Searchstaff);
    }

    _createClass(Searchstaff, [{
        key: "SearchstaffFromBranch",
        value: async function SearchstaffFromBranch(req, res) {
            console.log(req.body.brach_staff);
            await _staff2.default.find({
                "staff_branch": req.body.brach_staff
            }).then(function (staff) {
                res.status(200).send(staff);
            }).catch(function (err) {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
        }
    }, {
        key: "SearchstaffFromCollum",
        value: async function SearchstaffFromCollum(req, res) {
            console.log(req.body);
            await _staff2.default.find(req.body).then(function (staff) {
                res.status(200).send(staff);
            }).catch(function (err) {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
        }
    }]);

    return Searchstaff;
}();

exports.default = new Searchstaff();