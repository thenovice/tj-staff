"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _staff = require("../models/staff.model");

var _staff2 = _interopRequireDefault(_staff);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var staff = function () {
    function staff() {
        _classCallCheck(this, staff);
    }

    _createClass(staff, [{
        key: "getAll",
        value: async function getAll(req, res) {
            await _staff2.default.find().then(function (staff) {
                res.status(200).send(staff);
            }).catch(function (err) {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
        }
    }, {
        key: "findOne",
        value: async function findOne(req, res) {
            _staff2.default.findById(req.params.id).then(function (data) {
                if (!data) {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                res.send(data);
            }).catch(function (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving staff with id " + req.params.id
                });
            });
        }
    }, {
        key: "create",
        value: async function create(req, res) {
            var body = req.body;
            var Staff_ = new _staff2.default(body);
            Staff_.save().then(function (data) {
                res.send(data);
            }).catch(function (err) {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the Staff."
                });
            });
        }
    }, {
        key: "update",
        value: async function update(req, res) {
            if (!req.body.staff_fname) {
                return res.status(400).send({
                    message: "staff can not be empty"
                });
            }
            _staff2.default.findByIdAndUpdate(req.params.id, {
                staff_fname: req.body.staff_fname || "Untitled staff",
                staff_lname: req.body.staff_lname
            }, {
                new: true
            }).then(function (data) {
                if (!data) {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                res.send(data);
            }).catch(function (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error updating staff with id " + req.params.id
                });
            });
        }
    }, {
        key: "delete",
        value: async function _delete(req, res) {
            _staff2.default.findByIdAndRemove(req.params.id).then(function (data) {
                if (!data) {
                    return res.status(404).send({
                        message: "Staff not found with id " + req.params.id
                    });
                }
                res.send({
                    message: "Staff deleted successfully!"
                });
            }).catch(function (err) {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Could not delete Staff with id " + req.params.id
                });
            });
        }
    }]);

    return staff;
}();

exports.default = new staff();