"use strict";

var _staff = require("../models/staff.model");

var _staff2 = _interopRequireDefault(_staff);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Create and Save a new Note
create = function create(req, res) {
    // if(!req.body.content) {
    //     return res.status(400).send({
    //         message: "Note content can not be empty"
    //     });
    // }
    var staff = new _staff2.default({
        staff_fname: "Jaedsada",
        staff_lname: "Chaisorn"
    });

    // Save Note in the database
    staff.save().then(function (data) {
        res.send(data);
    }).catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Staff."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = function (req, res) {
    _staff2.default.find().then(function (notes) {
        res.send(notes);
    }).catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = function (req, res) {
    _staff2.default.findById(req.params.noteId).then(function (note) {
        if (!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send(note);
    }).catch(function (err) {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = function (req, res) {
    if (!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    _staff2.default.findByIdAndUpdate(req.params.noteId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, { new: true }).then(function (note) {
        if (!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send(note);
    }).catch(function (err) {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.noteId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = function (req, res) {
    _staff2.default.findByIdAndRemove(req.params.noteId).then(function (note) {
        if (!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send({ message: "Note deleted successfully!" });
    }).catch(function (err) {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};