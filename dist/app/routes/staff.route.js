'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _staff = require('../controller/staff.controller');

var _staff2 = _interopRequireDefault(_staff);

var _search = require('../controller/search.controller');

var _search2 = _interopRequireDefault(_search);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var StaffRoute = _express2.default.Router();

app.use(StaffRoute);

app.get('/staff', _staff2.default.getAll).post('/staff', _staff2.default.create).get('/staff/:id', _staff2.default.findOne).post('/search_frombranch/', _search2.default.SearchstaffFromBranch).post('/search_fromcollum/', _search2.default.SearchstaffFromCollum).put('/staff/update/:id', _staff2.default.update).delete('/staff/:id', _staff2.default.delete);

exports.default = app;