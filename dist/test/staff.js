'use strict';

var _staff2 = require('../app/models/staff.model');

var _staff3 = _interopRequireDefault(_staff2);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _chaiHttp = require('chai-http');

var _chaiHttp2 = _interopRequireDefault(_chaiHttp);

var _index = require('../index.js');

var _index2 = _interopRequireDefault(_index);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _chai2.default.expect;
// import server from '../server/app';

var should = _chai2.default.should();

_chai2.default.use(_chaiHttp2.default);

var _paramStaff = {
  "staff_department": "programmer",
  "staff_branch": "ตั้งใจดี",
  "staff_title": "นาย",
  "staff_fname": "เจษฎา",
  "staff_lname": "ไชยศร",
  "staff_gender": "ชาย",
  "staff_name": "ปืน",
  "information": {
    "email": "j.chaisorn3@gmail.com",
    "internal_phone": "076123456",
    "personal_phone": "0950413765"
  },
  "address": {
    "address": "asdf",
    "house_no": "12",
    "soi": "asdsa",
    "road": "asdsa",
    "country": "ไทย",
    "province_id": "ภูเก็ต",
    "district": "กะทู้",
    "sub_district_id": "กะทู้",
    "post_code": "83120"
  },
  "remark": "asdfsd"
};

describe('Staff', function () {

  before(function (done) {
    _staff3.default.remove({}, function (err) {
      done();
    });
  });

  describe('/GET All Function Staff', function () {
    it('staff succes', function (done) {
      _chai2.default.request(_index2.default).get('/api/v1/staff').end(function (err, res) {
        res.should.have.status(200);
        done();
      });
    });
  });

  describe('/GET Id Function Staff', function () {
    it('staff success', function (done) {
      var _staff = new _staff3.default(_paramStaff);
      _staff.save(function (err, data) {
        _chai2.default.request(_index2.default).get('/api/v1/staff/' + data.id).end(function (err, res) {
          res.should.have.status(200);
          // res.body.should.be.a('array');
          // res.body.length.should.be.eql(0);
          res.body.should.have.property('_id').eql(data.id);
          done();
        });
      });
    });
  });

  describe('/POST Search StaffFromBranch', function () {

    it('Search staff success', function (done) {
      var _staff = new _staff3.default(_paramStaff);
      _staff.save(function (err, data) {
        _chai2.default.request(_index2.default).post('/api/v1/search_fromcollum/').send({ "staff_branch": "ตั้งใจดี" }).end(function (err, res) {
          res.should.have.status(200);
          // res.body.should.be.a('array');
          // res.body.length.should.be.eql(0);
          // res.body.should.have.property('staff_branch').eql('ตั้งใจดี');
          done();
        });
      });
    });
  });

  describe('/POST Search StaffFromCollum', function () {

    it('Search staff success', function (done) {
      var _staff = new _staff3.default(_paramStaff);
      _staff.save(function (err, data) {
        _chai2.default.request(_index2.default).post('/api/v1/search_fromcollum/').send(_paramStaff).end(function (err, res) {
          res.should.have.status(200);
          // res.body.should.be.a('array');
          // res.body.length.should.be.eql(0);
          // res.body.should.have.property('staff_branch').eql('ตั้งใจดี');
          done();
        });
      });
    });
  });

  describe('/POST Function Staff', function () {
    it('post staff success', function (done) {
      _chai2.default.request(_index2.default).post('/api/v1/staff').send(_paramStaff).end(function (err, res) {
        if (err) res.should.have.status(200);
        done();
      });
    });
  });

  describe('/PUT Function Staff', function () {
    var _paramStaffUpdate = {
      "staff_department": "programmer1",
      "staff_branch": "ตั้งใจดี1",
      "staff_title": "นาย1",
      "staff_fname": "เจษฎา1",
      "staff_lname": "ไชยศร1",
      "staff_gender": "ชาย1",
      "staff_name": "ปืน1",
      "information": {
        "email": "j.chaisorn3@gmail.com1",
        "internal_phone": "07612345611",
        "personal_phone": "09504137651"
      },
      "address": {
        "address": "asdf1",
        "house_no": "121",
        "soi": "asdsa1",
        "road": "asdsa1",
        "country": "ไทย1",
        "province_id": "ภูเก็ต1",
        "district": "กะทู้1",
        "sub_district_id": "กะทู้1",
        "post_code": "83120"
      },
      "remark": "asdfsd"

    };
    it('put staff success', function (done) {
      var staff = new _staff3.default(_paramStaff);
      staff.save(function (err, result) {
        _chai2.default.request(_index2.default).put('/api/v1/staff/update/' + result.id).send(_paramStaffUpdate).end(function (err, res) {
          res.should.have.status(200);
          res.body.should.have.property('staff_fname').eql(_paramStaffUpdate.staff_fname);
          // res.body.staff_fname.have.eql(_paramStaffUpdate.staff_fname);
          done();
        });
      });
    });
    // it('put staff fail', (done) => {
    //   let staff = new Staff(_paramStaff)
    //   staff.save((err, result) => {
    //       chai.request(server)
    //       .put('/api/v1/staff/update/' + result.id)
    //       .send(_paramStaffUpdate)
    //       .end((err, res) => {
    //           res.should.have.status(200);
    //           res.body.should.have.property('staff_fname').eql(_paramStaffUpdate.staff_fname);
    //           // res.body.staff_fname.have.eql(_paramStaffUpdate.staff_fname);
    //           done();
    //       });
    //   });
    // });
  });

  describe('/DELETE Function Staff', function () {
    it('DELETE Staff success', function (done) {
      var staff = new _staff3.default(_paramStaff);
      staff.save(function (err, result) {
        _chai2.default.request(_index2.default).delete('/api/v1/staff/' + result.id).end(function (err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
      });
    });
  });
});