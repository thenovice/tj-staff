FROM node:10.0.0-alpine

RUN mkdir -p /var/www/html/node
WORKDIR /var/www/html/node

COPY package.json /var/www/html/node/
# RUN npm install -g nodemon
RUN npm install
# RUN npm install yarn --save -g
COPY . /var/www/html/node
EXPOSE 3001

CMD ["npm","start"]
# CMD [ "npm","install" ]
