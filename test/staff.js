import Staff from '../app/models/staff.model';

import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index.js';
import mongoose from 'mongoose';

const expect = chai.expect;
// import server from '../server/app';

const should = chai.should();

chai.use(chaiHttp);

let _paramStaff = {
  "staff_department": "programmer",
  "staff_branch": "ตั้งใจดี",
  "staff_title": "นาย",
  "staff_fname": "เจษฎา",
  "staff_lname": "ไชยศร",
  "staff_gender": "ชาย",
  "staff_name": "ปืน",
  "information": {
    "email": "j.chaisorn3@gmail.com",
    "internal_phone": "076123456",
    "personal_phone": "0950413765"
  },
  "address": {
    "address": "asdf",
    "house_no": "12",
    "soi": "asdsa",
    "road": "asdsa",
    "country": "ไทย",
    "province_id": "ภูเก็ต",
    "district": "กะทู้",
    "sub_district_id": "กะทู้",
    "post_code": "83120"
  },
  "remark": "asdfsd"
}

describe('Staff', () => {

  before(function (done) {
    Staff.remove({}, (err) => {
      done();
    });
  });

  describe('/GET All Function Staff', () => {
    it('staff succes', (done) => {
      chai.request(server)
        .get('/api/v1/staff')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe('/GET Id Function Staff', () => {
    it('staff success', (done) => {
      let _staff = new Staff(_paramStaff)
      _staff.save((err, data) => {
        chai.request(server)
          .get('/api/v1/staff/' + data.id)
          .end((err, res) => {
            res.should.have.status(200);
            // res.body.should.be.a('array');
            // res.body.length.should.be.eql(0);
            res.body.should.have.property('_id').eql(data.id);
            done();
          });
      });
    })
  });

  describe('/POST Search StaffFromBranch', () => {
    
    it('Search staff success', (done) => {
      let _staff = new Staff(_paramStaff)
      _staff.save((err, data) => {
        chai.request(server)
          .post('/api/v1/search_fromcollum/')
          .send({ "staff_branch" : "ตั้งใจดี" })
          .end((err, res) => {
            res.should.have.status(200);
            // res.body.should.be.a('array');
            // res.body.length.should.be.eql(0);
            // res.body.should.have.property('staff_branch').eql('ตั้งใจดี');
            done();
          });
      });
    });
  });

  describe('/POST Search StaffFromCollum', () => {
    
    it('Search staff success', (done) => {
      let _staff = new Staff(_paramStaff)
      _staff.save((err, data) => {
        chai.request(server)
          .post('/api/v1/search_fromcollum/')
          .send(_paramStaff)
          .end((err, res) => {
            res.should.have.status(200);
            // res.body.should.be.a('array');
            // res.body.length.should.be.eql(0);
            // res.body.should.have.property('staff_branch').eql('ตั้งใจดี');
            done();
          });
      });
    });
  });


  describe('/POST Function Staff', () => {
    it('post staff success', (done) => {
      chai.request(server)
        .post('/api/v1/staff')
        .send(_paramStaff)
        .end((err, res) => {
          if (err)
            res.should.have.status(200);
          done();
        });
    });
  });

  describe('/PUT Function Staff', () => {
    let _paramStaffUpdate = {
      "staff_department": "programmer1",
      "staff_branch": "ตั้งใจดี1",
      "staff_title": "นาย1",
      "staff_fname": "เจษฎา1",
      "staff_lname": "ไชยศร1",
      "staff_gender": "ชาย1",
      "staff_name": "ปืน1",
      "information": {
        "email": "j.chaisorn3@gmail.com1",
        "internal_phone": "07612345611",
        "personal_phone": "09504137651"
      },
      "address": {
        "address": "asdf1",
        "house_no": "121",
        "soi": "asdsa1",
        "road": "asdsa1",
        "country": "ไทย1",
        "province_id": "ภูเก็ต1",
        "district": "กะทู้1",
        "sub_district_id": "กะทู้1",
        "post_code": "83120"
      },
      "remark": "asdfsd"

    }
    it('put staff success', (done) => {
      let staff = new Staff(_paramStaff)
      staff.save((err, result) => {
        chai.request(server)
          .put('/api/v1/staff/update/' + result.id)
          .send(_paramStaffUpdate)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property('staff_fname').eql(_paramStaffUpdate.staff_fname);
            // res.body.staff_fname.have.eql(_paramStaffUpdate.staff_fname);
            done();
          });
      });
    });
    // it('put staff fail', (done) => {
    //   let staff = new Staff(_paramStaff)
    //   staff.save((err, result) => {
    //       chai.request(server)
    //       .put('/api/v1/staff/update/' + result.id)
    //       .send(_paramStaffUpdate)
    //       .end((err, res) => {
    //           res.should.have.status(200);
    //           res.body.should.have.property('staff_fname').eql(_paramStaffUpdate.staff_fname);
    //           // res.body.staff_fname.have.eql(_paramStaffUpdate.staff_fname);
    //           done();
    //       });
    //   });
    // });
  });

  describe('/DELETE Function Staff', () => {
    it('DELETE Staff success', (done) => {
      let staff = new Staff(_paramStaff)
      staff.save((err, result) => {
        chai.request(server)
          .delete('/api/v1/staff/' + result.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object')
            done();
          });
      });
    })
  })
})