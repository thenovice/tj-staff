import express from 'express'
import controller from '../controller/staff.controller'
import controllerSearch from '../controller/search.controller'

const app 	    = express();
var StaffRoute = express.Router()

app.use(StaffRoute);

app.get('/staff',controller.getAll)
   .post('/staff',controller.create)
   .get('/staff/:id',controller.findOne)
   .post('/search_frombranch/',controllerSearch.SearchstaffFromBranch)
   .post('/search_fromcollum/',controllerSearch.SearchstaffFromCollum)
   .put('/staff/update/:id',controller.update)
   .delete('/staff/:id',controller.delete)

export default app