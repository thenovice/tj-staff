import Staff from '../models/staff.model';

class staff {
    async getAll(req, res) {
        await Staff.find()
            .then(staff => {
                res.status(200).send(staff);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
    }

    async findOne(req, res) {
        Staff.findById(req.params.id)
            .then(data => {
                if (!data) {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                res.send(data);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving staff with id " + req.params.id
                });
            });
    }

    async create(req, res) {
        const body  = req.body
        const Staff_ = new Staff(body);
        Staff_.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the Staff."
                });
            });
    }

    async update(req, res) {
        if (!req.body.staff_fname) {
            return res.status(400).send({
                message: "staff can not be empty"
            });
        }
        Staff.findByIdAndUpdate(req.params.id, {
                staff_fname: req.body.staff_fname || "Untitled staff",
                staff_lname: req.body.staff_lname
            }, {
                new: true
            })
            .then(data => {
                if (!data) {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                res.send(data);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error updating staff with id " + req.params.id
                });
            });
    }

    async delete(req, res) {
        Staff.findByIdAndRemove(req.params.id)
            .then(data => {
                if (!data) {
                    return res.status(404).send({
                        message: "Staff not found with id " + req.params.id
                    });
                }
                res.send({
                    message: "Staff deleted successfully!"
                });
            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Staff not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Could not delete Staff with id " + req.params.id
                });
            });
    }
}

export default new staff;