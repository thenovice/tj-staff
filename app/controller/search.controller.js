import Staff from '../models/staff.model';

class Searchstaff {
    async SearchstaffFromBranch(req, res) {
        console.log(req.body.brach_staff);
        await Staff.find({
            "staff_branch": req.body.brach_staff
        }).then(staff => {
                res.status(200).send(staff);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
    }

    async SearchstaffFromCollum(req, res) {
        console.log(req.body);
        await Staff.find(req.body).then(staff => {
                res.status(200).send(staff);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
    }

}

export default new Searchstaff;