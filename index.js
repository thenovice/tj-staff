import express from 'express';
import bodyParser from 'body-parser';
import db from './config/server';
import staff from './app/routes/staff.route'
import morgan from 'morgan'

const app = express();
const path = '/api/v1/'


app.use(bodyParser.urlencoded({ extended: true }))
// app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(path,staff)

app.get('/', (req, res) => {
    res.json('Hello Staff');
});


const port = process.env.PORT || 3001;
if(!module.parent) {
    app.listen(port, () => {
        console.log("Server is listening on port 3003");
    });
}
export default app;
